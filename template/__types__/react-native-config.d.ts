declare module 'react-native-config' {
  export interface NativeConfig {
    // Add declarations for your .env variables here
    // API_KEY: string;
  }

  export const Config: NativeConfig;
  export default Config;
}
