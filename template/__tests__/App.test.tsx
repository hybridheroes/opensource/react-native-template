import 'react-native';
// Note: import explicitly to use the types shipped with jest.
import { it } from '@jest/globals';
import { act, render } from '@testing-library/react-native';
import React from 'react';

import App from '../src/App';

jest.useFakeTimers();

// We await 'create' and 'act' to avoid fail cases in certain asynchronous libraries
it('renders correctly', async () => {
  const view = render(<App />);
  await act(async () => {
    expect(view).toBeDefined();
  });
});
