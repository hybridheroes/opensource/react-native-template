const spacing = {
  '025': 2,
  '050': 4,
};

const radius = {
  '050': 4,
  '100': 8,
};

export default {
  spacing,
  radius,
};
