import { TextStyle } from 'react-native';

const familyTokens: Record<string, TextStyle['fontFamily']> = {};

const weightTokens: Record<string, TextStyle['fontWeight']> = {};

const sizeTokens: Record<string, TextStyle['fontSize']> = {};

const lineHeightTokens: Record<string, TextStyle['lineHeight']> = {};

export default {
  family: familyTokens,
  weight: weightTokens,
  size: sizeTokens,
  lineHeight: lineHeightTokens,
};
