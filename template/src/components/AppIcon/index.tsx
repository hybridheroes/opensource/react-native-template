import React, { FunctionComponent } from 'react';
import { Image, StyleSheet, ImageProps } from 'react-native';

export enum AppIconType {
  HYBRID_HEROES = require('./assets/hybrid_heroes.png'),
}

interface AppIconProps extends Omit<ImageProps, 'source'> {
  type: AppIconType;
}

const AppIcon: FunctionComponent<AppIconProps> = ({ type, style }) => (
  <Image style={[styles.icon, style]} source={type} />
);

export default AppIcon;

const styles = StyleSheet.create({
  icon: {
    height: 24,
    width: 24,
  },
});
