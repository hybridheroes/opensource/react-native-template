<picture>
  <img alt="Hybrid Heroes React Native Template" src="./docs/hero.webp" style="display: block; margin: 0 auto; max-width: 70%; height: auto;" />
</picture>


# :atom_symbol: Hybrid Heroes React Native Template

An extended template by [Hybrid Heroes](https://hybridheroes.de/). Elegant usage directly within the [React Native CLI](https://github.com/react-native-community/cli).

## :star: Extended features

- Extended ESLint & Prettier configuration.
- [Husky](https://typicode.github.io/husky) pre-commit setup
  - [lint-staged](https://github.com/okonet/lint-staged)
  - [commitlint](https://commitlint.js.org/)
- [Node Version Manager](https://github.com/nvm-sh/nvm) configuration file: ([`.nvmrc`](template/.nvmrc))
- [React Navigation](https://reactnavigation.org/) configuration.
- [Redux](https://redux.js.org/) configuration:
  - [Redux Toolkit](https://redux-toolkit.js.org/)
  - [React Redux](https://react-redux.js.org/)
  - [Redux Persist](https://github.com/rt2zz/redux-persist)
- [React Intl / Format.JS](https://formatjs.io/docs/react-intl/)

## :heavy_check_mark: Usage

```sh
npx @react-native-community/cli@latest init MyApp --pm npm --template https://gitlab.com/hybridheroes/opensource/react-native-template --install-pods true
```

**Note:** `yarn`, `npm`, or `bun` can be provided to the `--pm` option

## :warning: React Native CLI

This template only works with the new CLI. Make sure you have uninstalled the legacy `react-native-cli` first (`npm uninstall -g react-native-cli`) for the below command to work. If you wish to not use `npx`, you can also install the new CLI globally (`npm i -g @react-native-community/cli` or `yarn global add @react-native-community/cli`).

If you tried the above and still get the react-native-template-react- native-template-typescript: Not found error, please try adding the `--ignore-existing` flag to [force npx to ignore](https://github.com/npm/npx#description) any locally installed versions of the CLI and use the latest.

Further information can be found here: https://github.com/react-native-community/cli#about

# Documentation

## :arrow_up: Updating dependencies

For updating React Native, please refer to the [official React Native template](https://github.com/facebook/react-native/tree/main/packages/react-native/template). Don't forget to select the desired release tag.

For updating other dependencies, you can run the following command:

```
npm install @react-navigation/native@latest @react-navigation/native-stack@latest @reduxjs/toolkit@latest react-native-config@latest react-native-gesture-handler@latest react-native-mmkv@latest react-native-safe-area-context@latest react-native-screens@latest react-redux@latest redux-persist@latest @commitlint/cli@latest @commitlint/config-conventional@latest @formatjs/cli@latest husky@latest lint-staged@latest
```

## :gem: RVM & NVM

The Template comes with a `.nvmrc` (currently node 18) so you can use `nvm use` bash command.
You can also use `rvm use` bash command. The Ruby version is defined in the `Gemfile` (currently version 2.7.4)

## :computer: Contributing

Contributions are very welcome. Please check out the [contributing document](CONTRIBUTING.md).

## :bookmark: License

This project is [MIT](LICENSE) licensed.
